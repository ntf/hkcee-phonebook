package com.ntf.functions
{
	import mx.collections.ArrayCollection;
	
	/**
	 * A helper fo Numbers related functions
	 */
	public class Numbers
	{
		/**
		 * Array of numbers
		 */
		protected var array:Array;
		
		/**
		 * Constructor
		 * @param numbers array of the numbers
		 */
		public function Numbers(numbers:Array){
			this.array = numbers;
			
		}
		
		/**
		 * static function for create the numbers helper
		 */
		public static function Set(numbers:Array):Numbers{
			var obj:Numbers = new Numbers(numbers);
			return obj;
		}
		/**
		 * fast function for compare two number
		 */
		public static function Compare(n1:Number,n2:Number):Numbers{
			var array:Array = new Array();
			array[0]=n1;
			array[1]=n2;
			
			return new Numbers(array);
		}
		
		/**
		 * File the maximum in the numbers
		 */
		public function max():Number{
			var temp:Number =  this.array[0];
			for(var i:Number=1;i<this.array.length;i++){
				if(this.array[i]>temp && this.array[i]!=''){
					temp = this.array[i];
				}
			}
			return temp;
		}
		
		/**
		 * File the miniimum in the numbers
		 */
		public function min():Number{
			var temp:Number = this.array[0];
			
			for(var i:Number=1;i<this.array.length;i++){
				if(this.array[i]<temp  && this.array[i]!=''){
					temp = this.array[i];
				}
			}
			return temp;
		}
	}
}
