package com.ntf.helper
{
	import mx.collections.ArrayCollection;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.*;
	
	/**
	 * DataGrid Helper provide adding and deleting columns
	 */
	public class DataGridController
	{
		public var _datagrid:DataGrid;
		public static const COLUMN_EXISTS:String = 'COLUMN.EXISTS';
		public static const COLUMN_NOT_EXISTS:String ='COLUMN.NOT_EXISTS';
		
		public function DataGridController():void
		{
		}
		
		/**
		 * Get the columns of the DataGrid
		 * @return ArrayCollection of columns
		 */
		public function get Columns():ArrayCollection
		{
			return new ArrayCollection(this._datagrid.columns);
		}
		/**
		 * Add a column to the datagrid
		 * @param header the header title of the column
		 * @param datafield the datafiled of the column relate to the dataProvider
		 * @return Boolean status of the action
		 */
		public function addColumns(header:String,datafield:String):Boolean{
			var ac:ArrayCollection = new ArrayCollection(this._datagrid.columns);
			
			for each(var column:DataGridColumn in ac){
            	if(column.headerText == header){
            		return false;
            	}
          	} 
          	
          	var dgc:DataGridColumn = new DataGridColumn();
          	dgc.headerText = header;
          	dgc.dataField = datafield;
          	dgc.width = 100; 
          	
          	ac.addItemAt(dgc,ac.length);
          	this._datagrid.columns = ac.toArray();
          	return true;
		}
		
		/**
		 * edit a column of the datagrid
		 * @param column the column name
		 * @param new_header the new header name of the column
		 * @param new_datafield the new datafiled of the column relate to the dataProvider
		 * @return Boolean status of the action
		 */
		public function editColumns(column:DataGridColumn,new_header:String,new_datafield:String):void
		{
            if(new_header!=null){
            	column.headerText = new_header;
            }
            		
            if(new_datafield!=null){
            	column.dataField = new_datafield;
            }    		
		}

		/**
		 * delete a column of the datagrid
		 * @param header the title of the column
		 * @return Boolean status of the action
		 */
		public function deleteColumns(header:String):Boolean{
			var ac:ArrayCollection = new ArrayCollection(this._datagrid.columns);
			
			for each(var column:DataGridColumn in ac){
            	if(column.headerText == header){
            		ac.removeItemAt(ac.getItemIndex(column));
              		this._datagrid.columns = ac.toArray();	
            		return true;
            	}
          	} 

			return false;
		}
		
		/**
		 * set the sorable feature to the column
		 */
		public function sortable(column:DataGridColumn,status:Boolean):void
		{
			column.sortable = status;
		}
		

	}
}

