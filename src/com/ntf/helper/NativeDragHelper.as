package com.ntf.helper
{
	import flash.desktop.NativeDragManager;
	import flash.display.InteractiveObject;
	import flash.events.NativeDragEvent; 
	import flash.desktop.ClipboardFormats;
	
	/**
	 * A helper for NativeDrag
	 */
	public class NativeDragHelper
	{
		/**
		 * current dragging files
		 */
		public var dragFiles:Array = new Array();
		/**
		 * The UI Object allowed to drag
		 */
		public var stage:Object;
		
		public function NativeDragHelper(stage:Object)
		{
			this.stage = stage;
		}
		/**
		 * Register callback to Events
		 * @param enterHandler callback for enter the UI Object
		 * @pram dropHandler callback for drug in the UI Object
		 */
		public function regesterEventListener(enterHandler:Function,dropHandler:Function):void
		{
			this.stage.addEventListener(NativeDragEvent.NATIVE_DRAG_ENTER, enterHandler);
			this.stage.addEventListener(NativeDragEvent.NATIVE_DRAG_DROP, dropHandler);
		}
		
		/**
		 * Default event listener for enterHandler
		 */
		protected function enterApp(e:NativeDragEvent):void
		{
			if (e.clipboard.hasFormat(ClipboardFormats.FILE_LIST_FORMAT)){ //This is a file
				NativeDragManager.acceptDragDrop(this.stage as InteractiveObject);
			}
		}
		
		/**
		 * Default event listener for dropHandler
		 */
		protected function dropIn(e:NativeDragEvent):void
		{
			if (e.clipboard.hasFormat(ClipboardFormats.FILE_LIST_FORMAT)){ //This is a file
				this.dragFiles = e.clipboard.getData(ClipboardFormats.FILE_LIST_FORMAT) as Array;
			}
		}
	}
}