package com.ntf.PhoneBook
{
	
	import com.ntf.PhoneBook.ui.ApplicationUI;
	import com.ntf.helper.NativeDragHelper;
	
	import flash.desktop.ClipboardFormats;
	import flash.desktop.NativeDragManager;
	import flash.display.InteractiveObject;
	import flash.events.NativeDragEvent;
	import flash.filesystem.File;
	import com.ntf.Controls.Alert;
	import mx.events.FileEvent;
	/**
	 * Native Drag Hander of the phonebook 
	 */
	public class PBDragManager extends NativeDragHelper
	{
		public function PBDragManager(stage:ApplicationUI)
		{
			super(stage);
			this.regesterEventListener(this.enterHandler,this.dropHandler);
		}
		
		/**
		 * Event listener
		 */
		protected function enterHandler(e:NativeDragEvent):void
		{
			this.enterApp(e);
		}
		
		/**
		 * Event listener
		 */
		protected function dropHandler(e:NativeDragEvent):void
		{
			this.dropIn(e);
			var loader:PBLoader = Core.getInstance().loader;
			
			if(this.dragFiles.length > 1){ //Ensure only one file can be handle
				Alert.show('Only 1 file can be select');
				return;
         	}
			var file:File = this.dragFiles[0];
         	if(file.extension == "phonebook"){ //Check File Extension
         		loader.phoneBookLocation = file.nativePath;
				loader.open();	
	     	 }else{
	     	 	Alert.show('Unknown file extension: .'+ file.extension);     	
			}

		}
	}
}