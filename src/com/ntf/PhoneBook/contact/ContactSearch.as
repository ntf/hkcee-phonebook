package com.ntf.PhoneBook.contact
{
	import com.ntf.PhoneBook.contact.search.SearchParameters;
	import com.ntf.PhoneBook.event.SearchEvent;
	
	import flash.events.EventDispatcher;
	import flash.events.ProgressEvent;
	
	import mx.core.UIComponent;
	
	[Event(name=SearchEvent.SEARCH_COMPLETE, type="com.ntf.PhoneBook.event.SearchEvent")]
	[Event(name=ProgressEvent.PROGRESS, type="flash.events.ProgressEvent")]
	/**
	 * ContactSearch is a class for record searching amd work with SearchParameters. It require original data from The Singleton Object of ContactData and initialize an array (idList) for the searching. Searching would break down to several processes to prevent freezing of the program which affect the user interfaces during searching a large file. Sorting will do immediately after the Searching Process.
	 */
	public class ContactSearch extends EventDispatcher
	{
		protected var data:ContactData;
		protected var list:ContactList;
		protected var parameters:SearchParameters;	
		protected var idList:Array = new Array();
		protected var UI:UIComponent;
		protected var parametersCount:Number;
		protected var TotalAction:Number = 0;  
		protected var CurrentAction:Number = 0; 
		public var RecordPerIteration:Number = 500;
		
		public function ContactSearch(ui:UIComponent)
		{
			this.UI = ui as UIComponent; //for using the callLater method
			
			this.list = new ContactList(); 
			this.data = ContactData.getInstance();
			var contacts:Array = ContactData.getInstance().getContactsDataWithID();

			for each(var contact:Contact in contacts){
				this.idList.push(contact.id);
			}
			
			this.list.data = ContactData.getInstance().getContactsDataWithID();
		
		}	
		
		/**
		 * @param parameters SearchParameters
		 */ 
		public function search(parameters:SearchParameters):void
		{
			this.parameters = parameters;
			this.parametersCount = this.parameters.count;
			this._calculateAction();
			this.dispatchEvent(new ProgressEvent(ProgressEvent.PROGRESS, false, false, 0, this.TotalAction));
			
			for each(var parameter:Array in this.parameters.parameters){
				this.ParameterSearch(parameter);
			}
			
			if(this.parametersCount == 0){
				this.dispatchEvent(new SearchEvent(this.result)); // Dispatch Search Complete Event if there is no parameter.
			}
		}
		/**
		 * To calculate the Total action involve
		 */
		private function _calculateAction():void
		{
			this.TotalAction = this.parametersCount * this.idList.length;
		}
		
		/**
		 * Getter of the Search Result
		 * @return ContactList
		 */
		public function get result():ContactList
		{
			this.list.data = new Array();
			for each(var id:Number in this.idList){
				this.list.data.push(data.GetByID(id));
			}
			return this.list;
		}
		
		/**
		 * Parameter Search 
		 * @param parameter Array
		 * @return void
		 */
		public function ParameterSearch(parameter:Array):void
		{
			this._subParametereSearch(parameter,0);	
		}
		/**
		 * sub method for parameter search 
		 * @param parameter Array
		 * @param start Number
		 */
		private function _subParametereSearch(parameter:Array,start:Number):void
		{
			
			for (var i:Number = start;i < start + this.RecordPerIteration; i++){
				
				if( this.idList.hasOwnProperty(i) ){
					var result:Number = this.data.GetByID(this.idList[i]).getValueByColumn(parameter['parameter']).search(parameter['value']);
					
					if(this.data.GetByID(this.idList[i]).getValueByColumn(parameter['parameter']).search(parameter['value']) <= -1){ //delete method      		
						delete this.idList[i];
						
					}
					
				
				}
				
			}
			
			this._calculateAction();
			
			this.CurrentAction = this.CurrentAction + this.RecordPerIteration;
			if(this.CurrentAction < this.TotalAction){ // Dispatch a progress Event For Prgress Bar
				this.dispatchEvent(new ProgressEvent(ProgressEvent.PROGRESS, false, false, this.CurrentAction, this.TotalAction));
				this.UI.callLater(this._subParametereSearch,[parameter,i]);
			}else{
				this.dispatchEvent(new SearchEvent(this.result)); // finish
			}
		
		}



	}
}