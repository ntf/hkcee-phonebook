package com.ntf.PhoneBook.contact.export
{
	import com.ntf.PhoneBook.Core;
	import com.ntf.PhoneBook.contact.*;
	
	import flash.filesystem.*;
	/**
	 * Adapter for export html format
	 */
	public class ContactExportHTML extends ContactExport
	{
		/**
		 * File of the HTMLContainer
		 */
		protected var HTMLContainer:File;
		/**
		 * Content of the HTMLContainer
		 */
		protected var HTMLContainerContent:String;
		/**
		 * File of the ContainContainer
		 */
		protected var ContactContainer:File;
		/**
		 * Content of the ContactContainer
		 */
		protected var ContactContainerContent:String;
		
		public function ContactExportHTML()
		{
			super();
			super.fileExtension	= '.html';	 //set the fileExtension to the parent class	
		}
		
		/**
		 * Assign the template directory and read the content to the variable
		 * @param dir Directory of the template
		 */
		override public function templateDir(dir:String):void
		{
			super.templateDir(dir);//File.applicationDirectory.nativePath
			this.HTMLContainer = new File(dir + '/Container.html');
			this.ContactContainer = new File(dir + '/ContactContainer.html');
			var fileStream:FileStream;
			
			if(this.HTMLContainer.exists){
				fileStream = new FileStream();
				fileStream.open(this.HTMLContainer, FileMode.READ);
				this.HTMLContainerContent = fileStream.readUTFBytes(fileStream.bytesAvailable);
				fileStream.close();
			}
			
			if(this.ContactContainer.exists){
				fileStream = new FileStream();
				fileStream.open(this.ContactContainer, FileMode.READ);
				this.ContactContainerContent = fileStream.readUTFBytes(fileStream.bytesAvailable);
				fileStream.close();
			}
		}
		
		/**
		 * Start to create the export file
		 */
		override public function create():void
		{
			if(!this.HTMLContainer.exists || !this.ContactContainer.exists){
				throw Error('Template not found');
				return;
			}
			
			//Assign Template variables
			this.HTMLContainer_replace("{PHONEBOOK_FILE_NAME}",Core.getInstance().status);
			this.HTMLContainer_replace("{PHONEBOOK_VERSION}",Core.VERSION);
			
			var contactTemplate:String = new String(this.ContactContainerContent);
			var contactsHTML:String = '';
			for each(var contact:Contact in this.list.data){
				//downward compatibility
				this.ContactContainer_replace('{NAME}',contact.getValueByColumn('name'));
				this.ContactContainer_replace('{EMAIL}',contact.getValueByColumn('email'));
				this.ContactContainer_replace('{TEL_PHONE}',contact.getValueByColumn('phone'));
				this.ContactContainer_replace('{MOBILE_PHONE}',contact.getValueByColumn('mobile'));
				this.ContactContainer_replace('{ADDRESS}',contact.getValueByColumn('address'));
				this.ContactContainer_replace('{MSN}',contact.getValueByColumn('msn'));
				this.ContactContainer_replace('{YM}',contact.getValueByColumn('yahooMessenger'));
				this.ContactContainer_replace('{QQ}',contact.getValueByColumn('QQ'));
				this.ContactContainer_replace('{GOOGLETALK}',contact.getValueByColumn('GoogleTalk'));
				this.ContactContainer_replace('{AIM}',contact.getValueByColumn('aim'));
				this.ContactContainer_replace('{SKYPE}',contact.getValueByColumn('skype'));
				this.ContactContainer_replace('{FACEBOOK}',contact.getValueByColumn('facebook'));
				this.ContactContainer_replace('{OTHERS}',contact.getValueByColumn('others'));
				this.ContactContainer_replace('{NOTE}',contact.getValueByColumn('note'));
				
				for each(var field:Object in Core.getInstance().ContactsData.dataColumnList.source){
					this.ContactContainer_replace('{' + field.data +'}',contact.getValueByColumn(field.data));
				}
				
				contactsHTML += new String(this.ContactContainerContent) + "\n";
				this.ContactContainerContent = contactTemplate;
			}
			
			this.HTMLContainer_replace('{PHONEBOOK_CONTENT}',contactsHTML);
			this.createdData = this.HTMLContainerContent;
			
			
		}
		
		/**
		 * Helper for create export file
		 */
		protected function HTMLContainer_replace(replace_with:String,replace:String):void
		{
			this.HTMLContainerContent = this.str_replace(replace_with,replace,this.HTMLContainerContent);
		}
		
		/**
		 * Helper for create export file
		 */
		protected function ContactContainer_replace(replace_with:String,replace:String):void
		{
			this.ContactContainerContent = this.str_replace(replace_with,replace,this.ContactContainerContent);
		}
		
		/**
		 * Helper for create export file
		 */
		private function str_replace(replace_with:String,replace:String,  original:String ):String{
			var array:Array = original.split(replace_with);
			return array.join(replace);
		}
	}
}