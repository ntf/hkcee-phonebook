package com.ntf.PhoneBook.contact.export
{
	/**
	 * Adapter for export text file format
	 */
	public class ContactExportTXT extends ContactExportHTML
	{
		public function ContactExportTXT(Extension:String='.txt')
		{
			super();
			super.fileExtension = Extension;
		}
		

	}
}