package com.ntf.PhoneBook.contact.export
{
	import com.ntf.PhoneBook.Core;
	import com.ntf.PhoneBook.PBProperty;
	/**
	 * Adapter for export phonebook format
	 */
	public class ContactExportPhoneBook extends ContactExportHTML
	{
		public function ContactExportPhoneBook()
		{
			super();
			super.fileExtension = '.phonebook'; //Set file extension
		}

		/**
		 * Start to create the export file
		 */
		override public function create():void
		{
			super.create();
			//We should assign properties to the new phonebook
			super.HTMLContainer_replace('{PHONEBOOK_PROPERTIES}',Core.getInstance().property.GetXML().toXMLString());
			super.createdData = super.HTMLContainerContent;
			
			
		}
	}
}