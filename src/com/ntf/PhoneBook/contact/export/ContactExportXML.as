package com.ntf.PhoneBook.contact.export
{
	/**
	 * Adapter for export xml format
	 */
	public class ContactExportXML extends ContactExportHTML
	{
		public function ContactExportXML()
		{
			super();
			
			super.fileExtension = '.xml';
		}
		
	}
}