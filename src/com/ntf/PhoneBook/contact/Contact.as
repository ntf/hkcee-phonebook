package com.ntf.PhoneBook.contact
{
	import com.ntf.PhoneBook.Core;
	import com.ntf.PhoneBook.contact.*;
	
	/**
	 * Contact is a data model which provide easy to use interface of adding, modify or deleting a record.
	 */
	public class Contact
	{

		protected var isRecord:Boolean = false;
		/**
		 * XML Container
		 */
		public var recordData:XML=
	<contact id="">
      <name></name>
      <phone></phone>
      <mobile></mobile>
      <email></email>
      <address></address>
      <note></note>
      <msn></msn>
      <QQ></QQ>
      <yahooMessenger></yahooMessenger>
      <GoogleTalk></GoogleTalk>
      <AIM></AIM>
      <Skype></Skype>
    </contact>;
    
    	/**
    	 * Record ID of the data
    	 */
		protected var recordID:Number;
		public function Contact():void
		{
		}
		
		/**
		 * Add a new Record
		 * @return boolean
		 */
		public function NewRecord():Boolean{
			if(this.isRecord==true) return false;
			return true;
		}
		
		/**
		 * set the Data
		 * @param Data xml of the contact data
		 */
		public function setData(Data:XML):void{
			this.recordData = Data;
			this.recordID = Data.@id;
			this.isRecord = true;
		}
		
		/**
		 * Initializes a contact data
		 * @static
		 * @param xml xml of the contact data
		 * @return Contact
		 */
		public static function init(xml:XML):Contact{
			var contact:Contact = new Contact();
			contact.setData(xml);
			return contact;
		}
		
		/**
		 * Save the contact
		 */
		public function Save():void{
			if(this.isRecord == false){
				this.recordData.@id = ContactData.getInstance().getInsertID();
				ContactData.getInstance().rawData.contacts.appendChild(this.recordData);
				ContactData.getInstance().build();
			}
		}
		
		/**
		 * Delete the Contact
		 */
		public function Delete(build:Boolean=true):Boolean{
			if(this.isRecord==false) return false;
			delete this.recordData.parent().children()[this.recordData.childIndex()];
			
			if(build){
				ContactData.getInstance().build();
			}
			
			return true;
		}
		
		/**
		 * Getter of isRecord
		 */
		public function get isNewRecord():Boolean
		{
			return this.isRecord==false;
		}
		/**
		 * Getter of RecordID
		 */
		public function get id():Number{
			return this.recordID;
		}
		
		/**
		 * Get a column's value
		 * @param column name of the column
		 */
		public function getValueByColumn(column:String):String{
			return this.recordData[column];
		}
		
		/**
		 * Set a column's value
		 * @param column name of the column
		 * @param value  value of the required column
		 */
		public function setValueByColumn(column:String,value:String):void{
			this.recordData[column] = value;
		}
	}
}