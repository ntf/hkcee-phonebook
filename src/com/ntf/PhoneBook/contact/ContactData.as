package com.ntf.PhoneBook.contact
{
	import mx.collections.ArrayCollection;
	
	/**
	 * ContactData is an object for data initialization and data preparation. It provide interface for accessing the data of the XML. The search method will initialize ContactSearch for searching and returns an object of ContactList.
	 */
	public class ContactData
	{
		private static var _instance:ContactData = new ContactData();
		/**
		 * rawData (xml)
		 */
		public var rawData:XML;
		public var dataExists:Boolean = false;
		
		/**
		 * The Contacts data which array key is the contacts' id
		 */
		protected var ContactsDataKeyWithID:Array = new Array();
		/**
		 * The Contacts data
		 */
		public var ContactsData:Array = new Array();
		
		/**
		 * ArrayCollection of the data items used in the application
		 */
		[Bindable] public var dataColumnList:ArrayCollection;
		[Bindable] public var dataColumnListOthers:ArrayCollection;
		
		public function ContactData()
		{
			if (_instance != null){
                throw new Error("Singleton: Can not create instance by Constructer");
            }
		}
		
		/**
		 * Instance of Singleton Pattern
		 * @return ContactData
		 */ 
		public static function getInstance():ContactData
        {
            return _instance;
        }
        
        /**
         * Using new data for the object
         */
        public function newInstance(XMLData:XML):void
        {
        	ContactsDataKeyWithID = new Array();
        	ContactsData = new Array();
        	this.rawData = XMLData;
			this.dataExists = true;
			this.build();					
        }
        
        public function build():void
        {
        	ContactsDataKeyWithID = new Array();
        	ContactsData = new Array();
			for each (var contact:XML in this.rawData.contacts.contact){
				ContactsData.push(Contact.init(contact));
				ContactsDataKeyWithID[contact.@id] = Contact.init(contact);
			}	
			
        }
        /**
         * Get the insert id for the next contact
         */
        public function getInsertID():Number
        {
        	return this.ContactsDataKeyWithID.length;
        }
        
        /**
         * Get all contact data
         * @return ContactList
         */ 
		public function GetAll():ContactList{
			
			var list:ContactList = new ContactList();
			list.data = this.ContactsData;
			return list;
			
		}
		
		/**
		 * Simple search interface
		 * @return ContactList
		 */
		public function Search(Column:String,KeyWord:String):ContactList{
			 var list:ContactList = new ContactList();
			 for each(var contact:Contact in this.ContactsData){ 
         		if(contact.getValueByColumn(Column).search(KeyWord) > -1){
         			list.data[contact.id] = contact;
         		}
 			}
 			return list;	
		}
		
		/**
		 * Get a column by a ID
		 * @return Contact
		 */
		public function GetByID(contactID:Number):Contact{
			return this.ContactsDataKeyWithID[contactID];
		}
		
		/**
		 * Delete a column by a ID
		 */
		public function RemoveByID(contactID:Number):void{
			delete this.ContactsDataKeyWithID[contactID];
					
		}
		
		public function getContactsDataWithID():Array
		{
			return this.ContactsDataKeyWithID;
		}
		
		/**
		 * getter of rawData
		 */
		public function get rawXml():XML{
			return this.rawData;
		}
	}
}