package com.ntf.PhoneBook.contact.search
{
	/**
	 * Helper for ContactSearch
	 * This class is to contain all search parameters and need to assign to ContactSearch Object
	 */
	public class SearchParameters
	{
		protected var _parameters:Array = new Array();
		public var count:Number = 0;
		
		public function SearchParameters()
		{
		}
		
		public function get parameters():Array
		{
			return this._parameters;
		}
		/**
		 * Add a parameter to the object
		 * @param parameter String Name of the parameter
		 * @param value String
		 * @return SearchParameters
		 */
		public function add(parameter:String,value:String):SearchParameters
		{
			this.count ++;
			this._parameters[parameter] = new Array();
			this._parameters[parameter]['parameter']=parameter;
			this._parameters[parameter]['value']=value;
			return this;
		}
		
		/**
		 * Remove a parameter
		 * @return SearchParameters
		 */
		public function remove(parameter:String):SearchParameters
		{
			delete this._parameters[parameter];
			return this;
		}
	}
}