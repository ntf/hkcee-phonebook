package com.ntf.PhoneBook.contact
{
	import com.ntf.PhoneBook.contact.export.*;
	/**
	 * ContactExport - Factory Pattern 
	 */
	public class ContactExportFactory{
		/**
		 * Factory Pattern
		 * @param type string
		 * @return ContactExport
		 */
		static public function Get(type:String):ContactExport{
			switch(type) {
				case "html":
				return new ContactExportHTML();
				break;
				case "txt":
				return new ContactExportTXT();
				break;
				case "xml":
				return new ContactExportXML();
				break;
				case "phonebook":
				return new ContactExportPhoneBook();
				break;
				case "csv":
				return new ContactExportTXT('.csv');
				break;
				case "vCard":
				return new ContactExportTXT('.vcf');
				break;
			default:
				throw new ArgumentError("No Export Method : " + type);
			}
		}
	}
}