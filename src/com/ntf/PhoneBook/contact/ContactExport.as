package com.ntf.PhoneBook.contact
{
	/**
	 * ContactExport is an object provides export of data. It is design to use template to export the data
	 */
	public class ContactExport
	{
		import flash.filesystem.*;
		import flash.events.Event;
		public var data:ContactData;
		public var list:ContactList;
		protected var createdData:String;
		protected var fileExtension:String = '.txt';
		protected var dir:String;
		public function ContactExport()
		{	
		}
		
		public function create():void
		{
			
		}
		
		public function templateDir(dir:String):void
		{
			this.dir = dir;	
		}
		
		/**
		 * Ask user to save the file
		 * @param title String title of the window
		 */ 
		public function askForSave(title:String):void
		{
			
			var file:File = new File();
			file.cancel();
        	file.addEventListener(Event.SELECT, this._saveListener);  
        	file.browseForSave(title); 
		}
		/**
		 * Save file event listener
		 */
		protected function _saveListener(e:Event):void  
		{
            var location:File =  new File(e.target.nativePath + ('.'+ e.target.extension==this.fileExtension ? '' : this.fileExtension));
            var fileStream:FileStream = new FileStream();
			fileStream.open(location, FileMode.WRITE);
			fileStream.writeUTFBytes(this.createdData);
			fileStream.close(); 
			
  		}
	}
	
	
	
	
}