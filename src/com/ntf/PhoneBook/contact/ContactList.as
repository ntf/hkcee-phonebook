package com.ntf.PhoneBook.contact
{
	import flash.events.EventDispatcher;
	
	import flash.events.ProgressEvent;
	import mx.collections.ArrayCollection;
	import com.ntf.PhoneBook.Core;
	import com.ntf.PhoneBook.event.SortEvent;
	[Event(name=SortEvent.SEARCH_COMPLETE, type="com.ntf.PhoneBook.event.SortEvent")]
	[Event(name=ProgressEvent.PROGRESS, type="flash.events.ProgressEvent")]
	/**
	 *  ContactList is a collection of Contact and provide sorting function.
	 */ 
	public class ContactList extends EventDispatcher
	{	
		/**
		 * Array of Contact Object
		 */
		public var data:Array = new Array();
		protected var TotalAction:Number = 0;  
		protected var CurrentAction:Number = 0; 
		public var RecordPerIteration:Number = 5000;
		public function ContactList()
		{
			
		}
		
		/**
		 * return an ArrayCollection of the Contacts
		 * @return ArrayCollection
		 */
		public function getList():ArrayCollection{
			var array:ArrayCollection = new ArrayCollection();
			for each(var contact:Contact in this.data){
				array.addItem(contact.recordData);
			}
			return array;
		}
		
		/**
		 * Add a contact to the array
		 * @param contact Contact
		 * @return void
		 */
		public function add(contact:Contact):void{
			this.data.push(contact);
		}
		
		/**
		 * Sorting function for Contacts
		 * @param column Column Name
		 * @param ASCDESC Order By Type
		 * @return ContactList
		 */
		public function OrderBy(column:String,ASCDESC:String):void{
			//calculate total loop times
			this.TotalAction = this.data.length * this.data.length;
			this.dispatchEvent(new ProgressEvent(ProgressEvent.PROGRESS, false, false, this.CurrentAction, this.TotalAction));
			this._subSorting(column,ASCDESC,0,0);
		}
		
		/**
		 * Sorting Helper
		 */
		protected function _subSorting(column:String,ASCDESC:String,Currenti:Number,Currentn:Number):void{
			
			var temp:Contact,i:Number,n:Number,count:Number;
			count=0;
			
			switchLoop:
			switch(ASCDESC){
				case'ASC':
					for(i=Currenti; i < this.data.length-1 ;i++){
						
						for(n=Currentn;n < this.data.length-1 ; n++){
								count++;					
							if( this.data[n].getValueByColumn(column) > this.data[n+1].getValueByColumn(column)){
								temp = this.data[n];
								this.data[n] = this.data[n+1];
								this.data[n+1] = temp;
							}
							
							if(count >= this.RecordPerIteration){ //Break The loop here for split the process
								break switchLoop;
							}
						}
						Currentn=0;
						n=0;
					}
				break;
				case'DESC':
					for(i=Currenti; i < this.data.length-1;i++){
						for(n=Currentn;n < this.data.length-1 ; n++){
							count++;
							if( this.data[n].getValueByColumn(column) < this.data[n+1].getValueByColumn(column)){
								temp = this.data[n];
								this.data[n] = this.data[n+1];
								this.data[n+1] = temp;
							}
							
							if(count >= this.RecordPerIteration){ //Break The loop here for split the process
								break switchLoop;
							}
						}
						Currentn=0;
						n=0;
					}
				break;
			}
			
			this.CurrentAction = this.CurrentAction + this.RecordPerIteration;
			if(this.CurrentAction < this.TotalAction){
				this.dispatchEvent(new ProgressEvent(ProgressEvent.PROGRESS, false, false, this.CurrentAction, this.TotalAction));
				Core.getInstance().UI.callLater(this._subSorting,[column,ASCDESC,i,n]);
			}else{
				this.dispatchEvent(new SortEvent(this));
			}
			
		}
		
		
		
		
		
	}
}