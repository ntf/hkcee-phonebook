package com.ntf.PhoneBook.contact
{
	import com.ntf.PhoneBook.Core;
	
	import flash.filesystem.*;
	/**
	 * ContactFile is the class as an interface between the file and ContactData. It is used to open and read data to the ContactData and save the file when necessary. 
	 */
	public class ContactFile
	{
		/**
		 * File of the Required Phonebook
		 */
		protected var file:File;
		
		/**
		 * Constructor
		 * @param filelocation String
		 */
		public function ContactFile(filelocation:String)
		{
			this.file = new File(filelocation);
			
		}
		
		/**
		 * Is the file exists?
		 * @return Boolean
		 */ 
		public function exists():Boolean
		{
			return this.file.exists;
		}
		
		/**
		 * Get the file name
		 * @return file name
		 */
		public function fileName():String
		{
			return this.file.name;
		}
		
		/**
		 * Open the file and Pass data to ContactData
		 */
		public function open():void
		{
			if(this.file.exists){
				
				var fileStream:FileStream = new FileStream();
				fileStream.open(this.file, FileMode.READ);
				ContactData.getInstance().newInstance(XML(fileStream.readUTFBytes(fileStream.bytesAvailable)));
				fileStream.close();
			}else{
				Core.getInstance().ThrowError('Can not open the PhoneBook File','FILE_NOT_FOUND');
			}
		}
		
		/**
		 * Save the data of ContactData to the target file
		 */
		public function save():void
		{
			var fileStream:FileStream = new FileStream();
			fileStream.open(this.file, FileMode.WRITE);
			
			ContactData.getInstance().rawData.properties = Core.getInstance().property.GetXML();
			
			fileStream.writeUTFBytes(ContactData.getInstance().rawData);
			fileStream.close(); 
		}
	}
}