package com.ntf.PhoneBook
{
	import com.adobe.WindowBoundsManager;
	import com.ntf.PhoneBook.contact.*;
	import com.ntf.PhoneBook.ui.*;
	import com.ntf.functions.*;
	
	import flash.events.Event;
	
	import mx.core.Application;
	import mx.events.*;	
	
	[Event(name=Core.CONTACTLIST_MODE_CHANGE, type="flash.events.Event")]
	[Event(name=Core.CONTACTLIST_PHONEBOOK_CHANGE, type="flash.events.Event")]
	[Event(name=Core.CONTACTLIST_DATA_CHANGE, type="flash.events.Event")]
	[Event(name=Core.CORE_REFRESH_CONTACT_LIST, type="flash.events.Event")]
	
	/**
	 * This is the main class of the program.  Design Pattern Singleton Pattern used to ensure only one Core exists in the runtime. The Core Class should provide an interface for access all components of the application.
	 */
	public class Core
	{
		/**
		 * Singleton Instance
		 */
		private static var _instance:Core = new Core();
		/**
		 * ApplicationUI Instance
		 */
		public var UI:ApplicationUI;
		public static var ContactListMode:String;
		public var Controls:ControlHandler = new ControlHandler();
		/**
		 * Setting Array
		 */	
		[Bindable] private var Setting:Array = new Array();
		
		/** 
		 * Events
		 */
		 
		public static const CONTACTLIST_MODE_CHANGE:String = 'ContactList.ModeChange';
		public static const CONTACTLIST_DATA_CHANGE:String = 'ContactList.DataChange';
		public static const CONTACTLIST_PHONEBOOK_CHANGE:String = 'ContactList.PhoneBookChange';
		public static const CORE_REFRESH_CONTACT_LIST:String = 'Core.RefreshContactList';
				
		public var loader:PBLoader;
		public var property:PBProperty;
		protected var dragManager:PBDragManager;
		
		/**
		 * Object of ContactData
		 */
		[Bindable] public var ContactsData:ContactData;
		/**
		 * Object of ContactList
		 */
		[Bindable] public var ContactsList:ContactList;
		/**
		 * Status of the application
		 */
		[Bindable] public var status:String = 'Overview';
		
		/**
		 * Version of the application
		 */
		public static const VERSION:String = '1.0';
		
		public function Core()
		{
			if (_instance != null){
                throw new Error("Singleton: Can not create instance by Constructer");
            }

			this.ContactsList = new ContactList();
			this.Setting['ContactList.DisplayMode'] = 'Default';
		}
		
		public static function getInstance():Core
        {
            return _instance;
        }
	
		/**
		 * Build main User Interface
		 * @param main UI Object
		 * @param AppUI ApplicationUI (UI background)
		 */
		public function BuildUI(main:Application , AppUI:ApplicationUI):void
		{		
			WindowBoundsManager.getInstance().init( AppUI.stage.nativeWindow, AppUI ); //Build Auto Stage Resize
			this.UI = AppUI; 
			this.UI.init();		
			this.Dispatch();		
			main.visible=true; // make UI visible 

		}
		
		/**
		 * Application Data Dispatch
		 */
		public function Dispatch():void
		{
			this.ContactsData = ContactData.getInstance();
			this.loader = new PBLoader();
			this.property = new PBProperty();
			
			this.dragManager = new PBDragManager(this.UI);	
			
			this.setContactListMode(this.Setting['ContactList.DisplayMode']); //Set The Contact List Display Mode
		}
		
		/**
		 * Refesh Contacts Data
		 */
		public function RefeshContactList():void
		{
			this.UI.currentState='main';
			this.ContactsData = ContactData.getInstance();
			this.ContactsList = ContactData.getInstance().GetAll();
			this.DispatchEvent(new Event(Core.CORE_REFRESH_CONTACT_LIST));
			this.DispatchEvent(new Event(Core.CONTACTLIST_DATA_CHANGE)); //Event Displatch
		}
		
		/**
		 * Event Listener
		 */
		public function ContactDataEventListener(e:Event):void
		{
			this.RefeshContactList();
		}
		
		
		
		public function setContactListMode(Mode:String):void
		{
			Core.ContactListMode = Mode;
			dispatchEvent( new Event(Core.CONTACTLIST_MODE_CHANGE) );
		}
		
		/**
		 * DispatchEvent By Object
		 */
		public function DispatchEvent(e:Event):void
		{	
			dispatchEvent(e);
		}
		
		/**
		 * Error Handler
		 */
		public function ThrowError(Message:String,Code:String):void{
			
			this.UI.beforeState = this.UI.currentState;
			this.UI.currentState = 'error';
			this.addEventListener('ErrorDisplayer.Exit', this._ErrorReporterExit);
			this.UI.ErrorDisplay.Error(Message,Code);		
		}
		/**
		 * Error Reporter Exit Event Handler
		 */
		protected function _ErrorReporterExit(e:Event):void{
			 this.UI.currentState = this.UI.beforeState; 
		}
	}
}