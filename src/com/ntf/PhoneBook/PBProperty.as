package com.ntf.PhoneBook
{
	
	import com.ntf.PhoneBook.contact.*;
	
	import flash.events.Event;
	import flash.xml.XMLDocument;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.xml.SimpleXMLDecoder;
	import mx.rpc.xml.SimpleXMLEncoder;
	import mx.utils.ArrayUtil;
	import mx.utils.ObjectUtil;
	
	public class PBProperty
	{
		[Bindable]protected var properties:Object = new Object();
		[Bindable] protected var system:Core;
		protected var xml:XML;
		
		
		
		public function PBProperty()
		{
			this.system = Core.getInstance();
			this.system.addEventListener(Core.CONTACTLIST_PHONEBOOK_CHANGE,this.init);
		}

		public function destroy():void
		{
			this.properties = new Object();
			this.xml = new XML();
		}
		public function init(e:Event):void
		{
			this.destroy();
			this.xml = this.system.ContactsData.rawData;

			//Add Properties Here
			//The Class will save convert and save them as xml
			this.properties.Columns = this.ColumnsArrayCollection();
			this.system.ContactsData.dataColumnList = this.ColumnsArrayCollection();
			this.builddataColumnListOthers();

		}
		
		/**
		 * Buid up dataColumnListOthers
		 */
		private function builddataColumnListOthers():void
		{
			
			var ac:ArrayCollection = ObjectUtil.copy(this.system.ContactsData.dataColumnList) as ArrayCollection;
			for each(var Obj:Object in ac.list){
				if(Obj.hasOwnProperty('locked')){
					ac.removeItemAt(ac.getItemIndex(Obj));
				}
			}
			for(var i:Number=0;i<=4;i++){
				ac.removeItemAt(0); // Delete the item 0 four times
			}
			this.system.ContactsData.dataColumnListOthers = ac;
		}
		/**
		 * get a property
		 * @param property name of the property
		 */
		public function getProperty(property:String):String
		{
			return this.properties[property];
		}
		
		/**
		 * Synchronize the change in dataColumnList to dataColumnListOthers
		 */
		public function SyncArrayCollection():void
		{
			this.properties.Columns = this.system.ContactsData.dataColumnList;
			
			this.builddataColumnListOthers();
			system.DispatchEvent(new Event(Core.CONTACTLIST_DATA_CHANGE));
		}
		/**
		 * Set a property
		 * @param property name of the property
		 * @param value
		 */
		public function setProperty(property:String,value:Object):void
		{
			this.properties[property] = value;
		}
		
		/**
		 * Convert Xml to ArrayCollection
		 */
		private function ColumnsArrayCollection():ArrayCollection
		{
			var xml:XMLDocument = new XMLDocument( this.xml.properties.Columns );
			var decoder:SimpleXMLDecoder = new SimpleXMLDecoder();
			var data:Object = decoder.decodeXML( xml );		
			
			
			return new ArrayCollection( ArrayUtil.toArray(data.Columns.list.source.item) );
		
		}


		/**
		 * Convert Objects into XML
		 */
		public function GetXML():XML
        {
			var qName:QName = new QName("properties");
            var xmlDocument:XMLDocument = new XMLDocument();          
            var simpleXMLEncoder:SimpleXMLEncoder = new SimpleXMLEncoder(xmlDocument);
            simpleXMLEncoder.encodeValue(this.properties, qName, xmlDocument);
            var xml:XML = new XML(xmlDocument.toString());
          
            return xml;
       	}
	}
}