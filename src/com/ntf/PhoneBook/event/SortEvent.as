package com.ntf.PhoneBook.event
{
	import com.ntf.PhoneBook.contact.*;
	import flash.events.Event;
	/**
	 * Event for Sorting
	 */
	public class SortEvent extends Event
	{
		
		public static const SORT_COMPLETE:String = "SortComplete";
		public var SortResult:ContactList;
		public function SortEvent(Result:ContactList)
		{   
			this.SortResult = Result;
    		super(SortEvent.SORT_COMPLETE);
		}

	}
}