package com.ntf.PhoneBook.event
{
	import com.ntf.PhoneBook.contact.*;
	
	import flash.events.Event;
	
	/**
	 * Event for passing Contact Data
	 */
	public class ContactEvent extends Event{
	  			
		public var contact:Contact = new Contact();
	  	public var Type:String;
		public function ContactEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false):void
		{
			this.Type = type;
			super(type, bubbles, cancelable);
		}
		
	  	public function Model(contactObject:Contact):ContactEvent
	  	{
	  		this.contact = contactObject;
	  		return this;
	  	}
	  	
	}
}