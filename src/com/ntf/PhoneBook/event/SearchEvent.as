package com.ntf.PhoneBook.event
{
	import com.ntf.PhoneBook.contact.*;
	import flash.events.Event;
	/**
	 * Event for Searching
	 */
	public class SearchEvent extends Event
	{
		
		public static const SEARCH_COMPLETE:String = "SearchComplete";
		public var SearchResult:ContactList;
		public function SearchEvent(SearchResult:ContactList)
		{   
			this.SearchResult = SearchResult;
    		super(SearchEvent.SEARCH_COMPLETE);
		}

	}
}