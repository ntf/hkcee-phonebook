package com.ntf.PhoneBook
{
	import com.ntf.Controls.*;
	import com.ntf.PhoneBook.contact.*;
	
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.net.FileFilter;
	
	/**
	 * phonebook file loader. The object for load function and event listener 
	 */
	public class PBLoader
	{
		protected var system:Core = Core.getInstance();
		public var phoneBookLocation:String = new String();
		protected var fileExtension:String = '.phonebook';
		public var fileFilter:Array = [new FileFilter("PhoneBook(*.phonebook)", "*.phonebook"),new FileFilter("*","*.*")];
		public function PBLoader()
	
		{
			//this.system = system;
			//Event Listners
			system = Core.getInstance();
			//Menu Bar Event
			system.addEventListener('Open.file.save',this._save);
			system.addEventListener('Open.file.reload',this._reload);		
			system.addEventListener('Open.file.open',this._open);
			system.addEventListener('Open.file.new',this._newContact);
			system.addEventListener('Open.file.saveAs',this._saveAs);
			system.addEventListener('Open.file.close',this._close);
		}
		/**
		 * Open a phonebook file provided
		 */
		public function open():void
		{
			system.removeEventListener(Core.CONTACTLIST_DATA_CHANGE,this._save); //remove Auto Save
			var file:ContactFile = new ContactFile(this.phoneBookLocation);
			file.open();				
			system.status = file.fileName();
			system.property.destroy();
			
			system.RefeshContactList();
			system.DispatchEvent(new Event(Core.CONTACTLIST_PHONEBOOK_CHANGE));
			system.addEventListener(Core.CONTACTLIST_DATA_CHANGE,this._save); //Register Auto Save
		}
		
		/**
		 * Save the phonebook file provided
		 */
		public function save():void
		{
			var file:ContactFile = new ContactFile(this.phoneBookLocation);
			file.save();
		}
		
		protected function _close(e:Event):void
		{
			system.status='Overview';
			system.UI.changeState('overview');
			system.removeEventListener(Core.CONTACTLIST_DATA_CHANGE,this._save);
			system.property.destroy();
			system.DispatchEvent(new Event('DetailController.Remove'));
		}
		/**
		 * Save a blank data to the file
		 */
		protected function _newContact(e:Event):void{
			/*
			if(this.phoneBookLocation!=''){
				this.save(); //Save the current PhoneBook
			}
			*/
			this.phoneBookLocation=''; 
			/**
			 * Default XML
			 */
			ContactData.getInstance().rawData = 
	<phonebook>
	<contacts/>
	<properties>
    <Columns>
      <list>
        <source>
          <item>
            <data>name</data>
            <label>Name</label>
            <locked>true</locked>
          </item>
          <item>
            <data>phone</data>
            <label>Home Phone</label>
            <locked>true</locked>
          </item>
          <item>
            <data>mobile</data>
            <label>Mobile</label>
            <locked>true</locked>           
          </item>
          <item>
            <data>email</data>
            <label>Email</label>
            <locked>true</locked>       
          </item>
          <item>
            <data>address</data>
            <label>Address</label>
            <locked>true</locked>      
          </item>
          <item>
            <data>msn</data>
            <label>MSN</label>
          </item>
          <item>
            <data>yahooMessenger</data>
            <label>Y!M</label>         
          </item>
          <item>
            <data>GoogleTalk</data>
            <label>Google Talk</label>
          </item>
          <item>
            <data>QQ</data>
            <label>QQ</label>
          </item>
          <item>
            <data>Skype</data>
            <label>Skype</label>
          </item>
          <item>
            <data>Facebook</data>
            <label>Facebook</label>
          </item>
          <item>
            <data>others</data>
            <label>Others IM</label>
          </item>
        </source>
      </list>
        <source>
          <item>
            <data>name</data>
            <label>Name</label>
            <locked>true</locked>
          </item>
          <item>
            <data>phone</data>
            <label>Home Phone</label>
            <locked>true</locked>
          </item>
          <item>
            <data>mobile</data>
            <label>Mobile</label>
            <locked>true</locked>           
          </item>
          <item>
            <data>email</data>
            <label>Email</label>
            <locked>true</locked>       
          </item>
          <item>
            <data>address</data>
            <label>Address</label>
            <locked>true</locked>      
          </item>
          <item>
            <data>msn</data>
            <label>MSN</label>
          </item>
          <item>
            <data>yahooMessenger</data>
            <label>Y!M</label>         
          </item>
          <item>
            <data>GoogleTalk</data>
            <label>Google Talk</label>
          </item>
          <item>
            <data>QQ</data>
            <label>QQ</label>
          </item>
          <item>
            <data>Skype</data>
            <label>Skype</label>
          </item>
          <item>
            <data>Facebook</data>
            <label>Facebook</label>
          </item>
          <item>
            <data>others</data>
            <label>Others IM</label>
          </item>
        </source>
    </Columns>
  </properties>
  </phonebook>;
 			system.property.destroy();
  			ContactData.getInstance().build();
  			system.property.init(new Event('NewPhonebook')); 
  //Assign a blank data 
			 //Build the data first
			//No need to refesh
			var file:File = new File(); //Ask for save
        	file.addEventListener(Event.SELECT, this._saveListener);  
        	file.browseForSave('New Contact');
		}
		
		protected function _saveAs(e:Event):void{
			var file:File = new File();
        	file.addEventListener(Event.SELECT, this._saveListener);  
        	file.browseForSave('Save PhoneBook'); 
		}
		private function _saveListener(e:Event):void  
		{
            this.phoneBookLocation =  e.target.nativePath + (e.target.nativePath.match(this.fileExtension) ? '' : this.fileExtension);
            this.save();
            system.status = new File(this.phoneBookLocation as String).name;
            system.RefeshContactList();
  		}
		protected function _open(e:Event):void{
			var file:File = new File();
        	file.addEventListener(Event.SELECT, this._openListener);  
        	file.browseForOpen('Open PhoneBook',this.fileFilter); 
		}
		
		private function _openListener(e:Event):void  
		{
            this.phoneBookLocation =  e.target.nativePath as String;
            this.open();
  		}
  		
		protected function _save(e:Event):void{
			this.save();
		}
		
		protected function _reload(e:Event):void{
			var file:ContactFile = new ContactFile(this.phoneBookLocation);
			file.open();
			system.ContactsData.build();
			system.RefeshContactList();
		}
	}
}