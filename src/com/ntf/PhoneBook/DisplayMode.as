package com.ntf.PhoneBook
{
	import mx.events.*;
    import ApplcationControllBar;
    
    /**
     * Display mode setting (including in  the first draft of program)
     */
	public class DisplayMode
	{
		private var Mode:String;
		public function DisplayMode(Mode:String):void
		{
			this.setDisplayMode(Mode);
		}

		public function setDisplayMode(Mode:String):void
		{
			this.Mode = Mode;
		}
	}
}