package com.ntf.Controls
{
	import mx.controls.Alert;
	/**
	 * A interface for using Alert
	 */
	public class Alert extends mx.controls.Alert
	{
		function Alert()
		{
			    //mx.controls.Alert.okLabel='';
            	//mx.controls.Alert.cancelLabel='取消';
		}
		
		/**
		 * Show a Alert Box
		 * @param message The message of the Alert Box
		 * @param title The title of the Alert Box
		 */
		static public function show(message:String,title:String=""):void{
        	mx.controls.Alert.show(message,title);
        }
        
        /**
		 * Show a Alert Box for ask Yes No Question
		 * @param message The message of the Alert Box
		 * @param title The title of the Alert Box
		 * @param Callback Callback function when user choose a answer
		 */
		static public function ask(message:String,title:String="",Callback:Function=null):void{
        	mx.controls.Alert.show(message,title, 3, null, Callback);
        }
	}
}