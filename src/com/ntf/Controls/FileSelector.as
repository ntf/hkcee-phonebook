package com.ntf.Controls
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	
	import mx.controls.Button;
	import mx.controls.TextInput;
	/**
	 * FileSelector Control
	 */
	public class FileSelector extends Button
	{
		protected var title:String = 'Select a file';
		public var fileLocation:String = new String();
		public var filter:Array = new Array();
		public var target:TextInput;
		public function FileSelecter():void
		{
			this.addEventListener(MouseEvent.CLICK,this.openFileSelector);
		}
		
		/**
		 * set the selector button title
		 * @param Title title of the button
		 */
		public function set selectorTitle(Title:String):void
		{
			this.title = Title;
		}
		

		
		/**
		 * Open file event listener
		 */
		protected function openListener(e:Event):void  
		{
            this.fileLocation = e.target.nativePath;
            target.text = this.fileLocation;
            this.dispatchEvent(new Event('Selected'));
  		}
  		
  		/**
  		 * Open a dialog for brwse a file
  		 */
		public function openFileSelector():void
		{
			var file:File = new File();
        	file.addEventListener(Event.SELECT, this.openListener);  
        	file.browseForOpen(this.title,this.filter);  
		}
		
		

	}
}